## 开放平台-消息SDK接口说明
## 简介
 智慧助手第3方平台接口协议

## 接入说明

1.引入依赖包

>>
    <dependency>
       <groupId>com.msg</groupId>
       <artifactId>w-sdk</artifactId>
       <version>1.3.2</version>
    <dependency>
>>



2.使用

>>
    String MSG_SERVER_URL = "http://00000:8085/";
    MsgApiAuth auth = new MsgApiAuth("pbDWNKxx", "-68a621f19e4061643d34e0011661d9ds");
    MsgApiConfig config = new MsgApiConfig(MSG_SERVER_URL, auth);
    WhhMessageApiClient  WhhMessageApiClient = new  WhhMessageApiClient(config)
>>
URL 智慧助手消息平台  格式如下  http://www.xxx.com:888     

## 详细接口


1.关键词回复消息接入

>>
  CommonResponse<Void> pushKeywordMsg(KeywordMsgApiDto msg)
>>

请求参数：

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| msgId | String | 必填	消息ID；需要传入之前通知时的消息ID |
|0| tenantId | String | 必填	租户编号 默认为0 |
|0| userId | String | 必填	目标客户端用户ID |
|0| innId | String | String	非必需	推送该关键字的用户活动状态酒店id |
|0| title | String | 返必填	消息标题 |
|0| isNullMsg | Integer | 必填 	是否为空消息,无内容时传递；0,否;1,是 |
|0| data | String | 必填	消息内容[此处内容需匹配widget] |
|0| cancelable | Integer | 必填	消息是否可取消  |
|0| startTime | String | 必填	消息开始时间yyyy-MM-dd HH: mm:ss |
|0| endTime | String | 非必填	消息结束时间yyyy-MM-dd HH: mm:ss |

返回数据参数DATA说明： 空

2.用户维度通知消息接入

>>
  CommonResponse<Void> pushUserMsg(UserMsgApiDto msg) 
>>

请求参数：

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| userId | String | 必填	目标客户端用户ID |
|0| tenantId | String | 必填	租户编号 默认为0 |
|0| innId | String | String	非必需	推送该关键字的用户活动状态酒店id |
|0| templateId | String | 必填	之前通知时的消息ID |
|0| title | String | 返必填	消息标题 |
|0| data | String | 必填	消息内容[此处内容需匹配widget] |
|0| cancelable | Integer | 必填	消息是否可取消  |
|0| startTime | String | 必填	消息开始时间yyyy-MM-dd HH: mm:ss |
|0| endTime | String | 非必填	消息结束时间yyyy-MM-dd HH: mm:ss |

返回数据参数DATA说明： 

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| data | MsgResultDTO | 非必填	|
|1| msgId | String | 非必填	|
 
3.酒店维度通知消息接入(废弃)

>>
  CommonResponse<Void> pushInnMsg(InnMsgApiDto msg) 
>>

请求参数：

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| innId | String | 必填	推送该关键字的用户活动状态酒店id |
|0| tenantId | String | 必填	租户编号 默认为0 |
|0| templateId | String | 必填	之前通知时的消息ID |
|0| title | String | 返必填	消息标题 |
|0| data | String | 必填	消息内容[此处内容需匹配widget] |
|0| cancelable | Integer | 必填	消息是否可取消  |
|0| startTime | String | 必填	消息开始时间yyyy-MM-dd HH: mm:ss |
|0| endTime | String | 非必填	消息结束时间yyyy-MM-dd HH: mm:ss |
 
返回数据参数DATA说明： 

4.业务主动变更消息状态接入

>>
  CommonResponse<Void> updateMsgStatus(MsgStatusApiDTO msgStatusApiDTO)
>>

请求参数：

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| msgId | String | 必填	消息ID |
|0| msgStatus | Integer | 必填	消息状态(必须是2)  |


返回数据参数DATA说明： 空


## 基础部分

1.统一返回类封装

|层级| 参数名 |类型| 描述 |
|:------------:| :------------:|:---------:|:---------------:|
|0| code | String | 必填	响应码	|
|0| message | String | 非必填	响应消息	|
|0| data | String | 必填	数据内容 |

2.响应结果代码表

| code | message | 描述 |
|:------------:| :------------:|:---------:|
| 0 | 空 | 成功 | 	|
| -1 | 系统繁忙/其它异常 | 异常 |
| 40001 | 必填参数为空 |  |
| 40021 | appid 为空 |  |
| 40022 | token 为空 |  |

