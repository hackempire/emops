## 环境说明


**开发环境(nginx[上海]:00000)**


* 一、测试mock页面 

      1.消息主动推送模拟页地址
        http://00000:8085/msg/mock/

      2.soketio客户端模拟页地址
        http://00000:8086/client/soketio
  

* 二、socketio地址(桐超) 

      1.http://00000:8086
	
  

* 三、msg服务基础地址： http://00000:8085/msg 

      1.消息推送()
        http://00000:8085/msg/api/pushMsg

      2.widget资源下载()
        http://00000:8085/msg/widget/downLoad/{id}/{version}

        例子：http://00000:8085/msg/widget/downLoad/68d22e76-3424-4aad-9b2c-bd1bbfe84091/1.0.8
    
  
==================================================================================  

**测试环境(nginx[广州+上海]:00000)**


* 一、测试mock页面 

      1.消息主动推送模拟页地址
        http://00000:8085/msg/mock/

      2.soketio客户端模拟页地址
        http://00000:8086/client/soketio
  

* 二、socketio地址(桐超) 

      1.http://00000:8086
	
  

* 三、msg服务基础地址： http://00000:8085/msg 

      1.消息推送()
        http://00000:8085/msg/api/pushMsg

      2.widget资源下载()
        http://00000:8085/msg/widget/downLoad/{id}/{version}

        例子：http://00000:8085/msg/widget/downLoad/68d22e76-3424-4aad-9b2c-bd1bbfe84091/1.0.8
  
  
===================================================================================  
 
**生产环境** 

* 一、测试mock页面 

      1.消息主动推送模拟页地址
        https://w.com/msg/mock/

      2.soketio客户端模拟页地址
        https://w.com/client/soketio
  

* 二、socketio地址() 

      1.https://w.com
	
  

* 三、msg服务基础地址： https://w.com/msg 

      1.消息推送()
        https://w.com/msg/api/pushMsg

      2.widget资源下载()
        https://w.com/msg/widget/downLoad/{id}/{version}

        例子：https://w.com/msg/widget/downLoad/68d22e76-3424-4aad-9b2c-bd1bbfe84091/1.0.8