package com.empire.devops.emops;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * EmopsApplication class
 *
 * @author aaron
 * @date 2021/09/05
 */
@MapperScan("com.empire.devops.emops.*.mapper")
@SpringBootApplication
public class EmopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmopsApplication.class, args);
    }

}
