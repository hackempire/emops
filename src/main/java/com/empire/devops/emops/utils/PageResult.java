package com.empire.devops.emops.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 分页结果对象
 * @author aaron
 * @param <T> 结果集类型
 */
@ApiModel(value = "PageResult", description = "通用分页查询结果对象")
public class PageResult<T> {
	/**
	 * 分页列表
	 */
	@ApiModelProperty(value = "分页数据列表", name = "list")
	private List<T> list;

	/**
	 * 总条数
	 */
	@ApiModelProperty(value = "总条数", name = "count", example = "0", required = true)
	private Integer count;

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


}
