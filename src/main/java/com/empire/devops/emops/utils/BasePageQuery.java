package com.empire.devops.emops.utils;

import io.swagger.annotations.ApiModelProperty;

/**
 * 基础查询封装
 *
 * @author aaron
 */
public class BasePageQuery implements java.io.Serializable {
    /**
     * 开始页面
     */
    @ApiModelProperty(value = "当前页", name = "page", dataType = "int", example = "0", required = true)
    private Integer page;
    /**
     * 页面大小
     */
    @ApiModelProperty(value = "页面大小", name = "pageSize", dataType = "int", example = "10", required = true)
    private Integer pageSize;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}