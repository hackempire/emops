package com.empire.devops.emops.utils;

import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * ElasticSearchUtil class
 *
 * @author aaron
 * @date 2021/09/05
 */
public class ElasticSearchUtil {
    private static Logger logger = LoggerFactory.getLogger(ElasticSearchUtil.class);

    /**
     * 通过全路径类名称获取对应的Class<?>类
     * @param clazzName 全路径类名称
     * @return Class<?>
     */
    public static Class<?> getClazz(String clazzName) {
        try {
            return Class.forName(clazzName);
        } catch (ClassNotFoundException e) {
            logger.error("getClazz()",e.getMessage());
            return null;
        }
    }

    /**
     *
     * @param queryBuilder 设置查询对象
     * @param from 设置from选项，确定要开始搜索的结果索引。 默认为0
     * @param size 设置大小选项，确定要返回的搜索匹配数。 默认为10
     * @param timeout 超时时间
     * @return SearchSourceBuilder
     */
    public static SearchSourceBuilder initSearchSourceBuilder(QueryBuilder queryBuilder, int from, int size,
        int timeout) {
        // 使用默认选项创建 SearchSourceBuilder 。
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 设置查询对象。可以使任何类型的 QueryBuilder
        sourceBuilder.query(queryBuilder);
        // 设置from选项，确定要开始搜索的结果索引。 默认为0。
        sourceBuilder.from(from);
        // 设置大小选项，确定要返回的搜索匹配数。 默认为10。
        sourceBuilder.size(size);
        sourceBuilder.timeout(new TimeValue(timeout, TimeUnit.SECONDS));
        return sourceBuilder;
    }

    /**
     * 初始化查询对象构建
     * @param queryBuilder 查询构建器
     * @return SearchSourceBuilder
     */
    public static SearchSourceBuilder initSearchSourceBuilder(QueryBuilder queryBuilder) {
        return initSearchSourceBuilder(queryBuilder, 0, 10, 60);
    }
}
