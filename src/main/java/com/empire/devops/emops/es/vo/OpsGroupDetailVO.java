package com.empire.devops.emops.es.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.empire.devops.emops.es.entity.OpsGroupEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @author aaron
 * @Date 2023/1/1
 */
@Data
@ApiModel(value = "OpsGroupDetailVO", description = "项目分组详细对象")
public class OpsGroupDetailVO {

    /**
     * 项目组ID
     */
    @ApiModelProperty(value = "id", name = "id", example = "1", required = true)
    private Integer id;

    /**
     * 分组名称
     */
    @ApiModelProperty(value = "项目分组名称", name = "groupName", example = "", required = true)
    private String groupName;

    /**
     * 邮件，多个使用逗号分隔
     */
    @ApiModelProperty(value = "邮件地址", name = "email", example = "", required = true)
    private String email;

    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", name = "remark", example = "", required = true)
    private String remark;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "createTime", example = "", required = true)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者", name = "createBy", example = "", required = false)
    private String createBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", name = "updateTime", example = "", required = false)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者", name = "updateBy", example = "", required = false)
    private String updateBy;

    public static OpsGroupDetailVO newInstance(OpsGroupEntity opsGroupEntity) {
        OpsGroupDetailVO opsGroupDetailVO = new OpsGroupDetailVO();
        BeanUtils.copyProperties(opsGroupEntity, opsGroupDetailVO);
        return opsGroupDetailVO;
    }
}
