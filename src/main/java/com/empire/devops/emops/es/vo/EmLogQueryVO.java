package com.empire.devops.emops.es.vo;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.annotation.JSONField;
import com.empire.devops.emops.utils.BasePageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * QueryVO class 查詢對象
 *
 * @author aaron
 * @date 2021/09/05
 */
@ApiModel(value = "EmLogQueryVO", description = "日志查询对象")
public class EmLogQueryVO extends BasePageQuery {
    /**
     * app name
     */
    @ApiModelProperty(value = "项目名", name = "appName", example = "\"APP\"", required = true)
    private String appName;
    /**
     * 日志日期
     */
    @ApiModelProperty(value = "日志日期", name = "idxDate", example = "\"2021-09-07\"", required = true)
    private String idxDate;
    /**
     * 日志级别
     */
    @ApiModelProperty(value = "日志级别", name = "level", example = "\"INFO,DEBUG,WARN,ERROR\"")
    private String level;
    /**
     * 日志级别
     */
    @ApiModelProperty(value = "日志内容", name = "message")
    private String message;

    /**
     * 开始时间时间戳，服务器时间
     */
    @ApiModelProperty(value = "开始时间时间戳，服务器时间;格式:yyyy-MM-dd HH:mm:ss", name = "startTime",
            example = "2020-03-17 18:00:00")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 结束时间时间戳，服务器时间，如果有结束时间，则为一个限时消息
     */
    @ApiModelProperty(value = "结束时间时间戳，服务器时间，如果有结束时间，则为一个限时消息;格式:yyyy-MM-dd HH:mm:ss", name = "endTime"
            , example = "2020-03-18 18:00:00")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;


    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getIdxDate() {
        return idxDate;
    }

    public void setIdxDate(String idxDate) {
        this.idxDate = idxDate;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 获取es索引
     *
     * @return es索引列表
     */
    public List<String> getIdx() {
        List<String> idxList = new ArrayList<>();
        if (StringUtils.hasLength(this.getIdxDate())) {
            List<String> idxDateList = Arrays.asList(this.getIdxDate().split(","));
            idxDateList.forEach(item -> {
                item = this.getAppName() + "-" + item;
                idxList.add(item);

            });
            return idxList;
        }
        //索引封装【根据时间】
        if (this.getStartTime() != null && this.getEndTime() != null) {
            //相差多少天
            long betweenDay = DateUtil.between(this.getStartTime(), this.getEndTime(), DateUnit.DAY);
            for (int i = 0; i <= betweenDay; i++) {
                Date date = DateUtil.offset(this.getStartTime(), DateField.DAY_OF_MONTH, i);
                String idxDate = DateUtil.formatDate(date);
                String idxItem = this.getAppName() + "-" + idxDate;
                idxList.add(idxItem);
            }
        }
        return idxList;
    }
}
