package com.empire.devops.emops.es.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.empire.devops.emops.es.entity.OpsGroupProjectEntity;
import com.empire.devops.emops.es.entity.OpsProjectEntity;
import com.empire.devops.emops.es.vo.OpsGroupProjectInfoVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryByGroupVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryPageByGroupVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Mapper
public interface OpsGroupProjectMapper extends BaseMapper<OpsGroupProjectEntity> {
    /**
     * 分页查询组内项目列表
     *
     * @param opsGroupProjectQueryPageByGroupVO 项目组项目列表查询对象
     * @return List<OpsProjectEntity> 返回项目组不包含的项目列表
     */
    List<OpsProjectEntity> queryExcludeListByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO);

    /**
     * 查询非组内项目列表总条数
     *
     * @param opsGroupProjectQueryPageByGroupVO 项目组项目列表查询对象
     * @return 总条数
     */
    Long queryExcludeCountByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO);

    /**
     * 分页查询组内项目列表
     *
     * @param osGroupProjectQueryByGroupVO 项目组项目列表查询对象
     * @return List<OpsGroupProjectInfoVO> 返回项目组包含的项目列表
     */
    List<OpsGroupProjectInfoVO> queryIncludeListByGroup(OpsGroupProjectQueryByGroupVO osGroupProjectQueryByGroupVO);

}
