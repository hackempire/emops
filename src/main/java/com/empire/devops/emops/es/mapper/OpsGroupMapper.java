package com.empire.devops.emops.es.mapper;

import com.empire.devops.emops.es.entity.OpsGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Mapper
public interface OpsGroupMapper extends BaseMapper<OpsGroupEntity> {

}
