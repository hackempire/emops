package com.empire.devops.emops.es.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * QueryVO class 查詢對象
 *
 * @author aaron
 * @date 2021/09/05
 */
@ApiModel(value = "QueryVO", description = "es通用查询对象")
public class QueryVO {
    /**
     * 索引名
     */
    @ApiModelProperty(value = "索引名", name = "idxName",  example = "\"APP-2020-09-07\"")
    private String idxName;
    /**
     * 响应类名
     */
    @ApiModelProperty(value = "相应类名", name = "className",  example = "\"APP-2020-09-07\"")
    private String className;
    /**
     * 查询Map封装
     */
    private Map<String, Map<String, Object>> query;

    public String getIdxName() {
        return idxName;
    }

    public void setIdxName(String idxName) {
        this.idxName = idxName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Map<String, Map<String, Object>> getQuery() {
        return query;
    }

    public void setQuery(Map<String, Map<String, Object>> query) {
        this.query = query;
    }
}
