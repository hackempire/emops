package com.empire.devops.emops.es.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.empire.devops.emops.es.entity.OpsGroupProjectEntity;
import com.empire.devops.emops.es.entity.OpsProjectEntity;
import com.empire.devops.emops.es.mapper.OpsGroupProjectMapper;
import com.empire.devops.emops.es.service.IOpsGroupProjectService;
import com.empire.devops.emops.es.vo.OpsGroupProjectInfoVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryByGroupVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryPageByGroupVO;
import com.empire.devops.emops.es.vo.OpsProjectDetailVO;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Service
public class OpsGroupProjectServiceImpl extends ServiceImpl<OpsGroupProjectMapper, OpsGroupProjectEntity> implements IOpsGroupProjectService {

    @Override
    public List<OpsProjectDetailVO> queryExcludeListByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO) {
        List<OpsProjectDetailVO> rs = new ArrayList<>();
        List<OpsProjectEntity> list = this.baseMapper.queryExcludeListByGroup(opsGroupProjectQueryPageByGroupVO);
        if (!CollectionUtils.isEmpty(list)) {
            list.forEach(item -> rs.add(OpsProjectDetailVO.newInstance(item)));
        }
        return rs;
    }

    @Override
    public Long queryExcludeCountByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO) {
        return this.baseMapper.queryExcludeCountByGroup(opsGroupProjectQueryPageByGroupVO);
    }

    @Override
    public List<OpsGroupProjectInfoVO> queryIncludeListByGroup(OpsGroupProjectQueryByGroupVO osGroupProjectQueryByGroupVO) {
        return this.baseMapper.queryIncludeListByGroup(osGroupProjectQueryByGroupVO);
    }
}
