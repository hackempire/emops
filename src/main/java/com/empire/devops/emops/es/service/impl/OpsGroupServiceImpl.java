package com.empire.devops.emops.es.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.empire.devops.emops.es.entity.OpsGroupEntity;
import com.empire.devops.emops.es.mapper.OpsGroupMapper;
import com.empire.devops.emops.es.service.IOpsGroupService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Service
public class OpsGroupServiceImpl extends ServiceImpl<OpsGroupMapper, OpsGroupEntity> implements IOpsGroupService {

}
