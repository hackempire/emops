package com.empire.devops.emops.es.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author aaron
 * @Date 2022/12/28
 */
@Data
@ApiModel(value = "BasePageQueryVO", description = "通用分页查询对象")
public class BasePageQueryVO {
    /**
     * 开始页面
     */
    @ApiModelProperty(value = "开始页", name = "from", example = "1", required = true)
    private Integer from;
    /**
     * 页面大小
     */
    @ApiModelProperty(value = "页面大小", name = "size",  example = "10", required = true)
    private Integer size;
}
