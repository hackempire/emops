package com.empire.devops.emops.es.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.empire.devops.emops.es.entity.OpsProjectEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author aaron
 * @Date 2022/12/27
 */
@Data
@ApiModel(value = "OpsProjectDetailVO", description = "项目详细对象")
public class OpsProjectDetailVO implements Serializable {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "id", name = "id", example = "1", required = false)
    private Integer id;

    /**
     * 项目英文名
     */
    @ApiModelProperty(value = "项目英文名称", name = "appName", example = "log", required = true)
    private String appName;

    /**
     * 项目中文名
     */
    @ApiModelProperty(value = "项目中文名称", name = "name", example = "项目名", required = true)
    private String name;
    /**
     * 有效期
     */
    @ApiModelProperty(value = "日志保留有效期（默认七天）", name = "expired", example = "7", required = true)
    private Integer expired;
    /**
     * 类型：1.日志集成;2.监控集成;3.日志+监控集成
     */
    @ApiModelProperty(value = "项目类型：1.日志集成;2.监控集成;3.日志+监控集成", name = "type", example = "1", required = true)
    private Integer type;

    /**
     * 健康监控URL
     */
    @ApiModelProperty(value = "健康监控URL", name = "healthUrl", example = "http://localhost:80/health", required = false)
    private String healthUrl;
    /**
     * IP地址，多个使用,分割
     */
    @ApiModelProperty(value = "IP地址,多个使用','分割", name = "ipAddress", example = "127.0.0.1:80", required = false)
    private String ipAddress;
    /**
     * 健康状态：1.正常,2.异常
     */
    @ApiModelProperty(value = "健康状态:1.正常,2.异常", name = "status", example = "1", required = false)
    private Integer status;
    /**
     * 健康监控正则表达式
     */
    @ApiModelProperty(value = "健康监控正则表达式", name = "cron", example = "0 0/3 * * * ?", required = false)
    private String cron;
    /**
     * 异常信息
     */
    @ApiModelProperty(value = "异常信息", name = "msg", example = "", required = false)
    private String msg;
    /**
     * 备注
     */
    @ApiModelProperty(value = "项目备注", name = "remark", example = "", required = false)
    private String remark;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "createTime", example = "", required = true)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者", name = "createBy", example = "", required = false)
    private String createBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", name = "updateTime", example = "", required = false)
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者", name = "updateBy", example = "", required = false)
    private String updateBy;

    public static OpsProjectDetailVO newInstance(OpsProjectEntity opsProjectEntity) {
        OpsProjectDetailVO opsProjectDetailVO=new OpsProjectDetailVO();
        BeanUtils.copyProperties(opsProjectEntity,opsProjectDetailVO);
        return opsProjectDetailVO;
    }
}
