package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.utils.BasePageQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author aaron
 * @Date 2023/1/1
 */
@Data
public class OpsGroupQueryVO extends BasePageQuery implements Serializable {
    /**
     * 分组名称
     */
    @ApiModelProperty(value = "项目分组名称", name = "groupName", example = "", required = true)
    private String groupName;

    /**
     * 邮件，多个使用逗号分隔
     */
    @ApiModelProperty(value = "邮件地址", name = "email", example = "", required = true)
    private String email;

    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", name = "remark", example = "", required = true)
    private String remark;
}
