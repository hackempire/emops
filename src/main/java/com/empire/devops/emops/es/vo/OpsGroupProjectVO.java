package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.es.entity.OpsGroupProjectEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aaron
 * @Date 2023/1/1
 */
@Data
@ApiModel(value = "OpsGroupProjectVO", description = "新增/更新项目组信息对象")
public class OpsGroupProjectVO {
    @ApiModelProperty(value = "项目分组ID", name = "groupId", example = "1", required = true)
    private Integer groupId;
    @ApiModelProperty(value = "项目ID列表", name = "projectList", example = "", required = true)
    private List<Integer> projectList;

    public List<OpsGroupProjectEntity> toOpsGroupProjectList() {
        List<OpsGroupProjectEntity> opsGroupProjectList = new ArrayList<>();
        projectList.forEach(item -> {
            OpsGroupProjectEntity opsGroupProject = new OpsGroupProjectEntity();
            opsGroupProject.setGroupId(this.groupId);
            opsGroupProject.setProjectId(item);
            opsGroupProjectList.add(opsGroupProject);
        });
        return opsGroupProjectList;
    }
}
