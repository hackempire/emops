package com.empire.devops.emops.es.mapper;

import com.empire.devops.emops.es.entity.OpsProjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xutenglong
 * @since 2022-12-26
 */
@Mapper
public interface OpsProjectMapper extends BaseMapper<OpsProjectEntity> {

}
