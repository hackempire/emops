package com.empire.devops.emops.es.service.impl;

import com.empire.devops.emops.es.entity.OpsProjectEntity;
import com.empire.devops.emops.es.mapper.OpsProjectMapper;
import com.empire.devops.emops.es.service.IOpsProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xutenglong
 * @since 2022-12-26
 */
@Service
public class OpsProjectServiceImpl extends ServiceImpl<OpsProjectMapper, OpsProjectEntity> implements IOpsProjectService {

}
