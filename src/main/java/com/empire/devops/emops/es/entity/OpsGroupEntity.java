package com.empire.devops.emops.es.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Getter
@Setter
@TableName("ops_group")
public class OpsGroupEntity extends Model<OpsGroupEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 项目组ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分组名称
     */
    @TableField("group_name")
    private String groupName;

    /**
     * 邮件，多个使用逗号分隔
     */
    @TableField("email")
    private String email;

    /**
     * 备注信息
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
