package com.empire.devops.emops.es.service;

import com.empire.devops.emops.es.entity.OpsProjectEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xutenglong
 * @since 2022-12-26
 */
public interface IOpsProjectService extends IService<OpsProjectEntity> {

}
