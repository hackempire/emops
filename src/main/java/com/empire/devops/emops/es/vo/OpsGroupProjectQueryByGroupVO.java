package com.empire.devops.emops.es.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author aaron
 * @Date 2022/12/27
 */
@Data
@ApiModel(value = "OpsGroupProjectQueryByGroupVO", description = "根据项目组查询项目的查询VO对象")
public class OpsGroupProjectQueryByGroupVO implements Serializable {
    /**
     * 关键词查询
     */
    @ApiModelProperty(value = "关键词查询", name = "key", example = "log", required = false)
    private String key;
    /**
     * 项目分组ID
     */
    @ApiModelProperty(value = "项目分组ID", name = "groupId", example = "1", required = true)
    private Integer groupId;
}
