package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.es.entity.OpsProjectEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author aaron
 * @Date 2022/12/27
 */
@Data
@ApiModel(value = "OpsProjectVO", description = "新增项目/更新对象")
public class OpsProjectVO implements Serializable {
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "id", name = "id", example = "1", required = false)
    private Integer id;

    /**
     * 项目英文名
     */
    @ApiModelProperty(value = "项目英文名称", name = "appName", example = "log", required = true)
    private String appName;

    /**
     * 项目中文名
     */
    @ApiModelProperty(value = "项目中文名称", name = "name", example = "项目名", required = true)
    private String name;
    /**
     * 有效期
     */
    @ApiModelProperty(value = "日志保留有效期（默认七天）", name = "expired", example = "7", required = true)
    private Integer expired;
    /**
     * 类型：1.日志集成;2.监控集成;3.日志+监控集成
     */
    @ApiModelProperty(value = "项目类型：1.日志集成;2.监控集成;3.日志+监控集成", name = "type", example = "1", required = true)
    private Integer type;

    /**
     * 健康监控URL
     */
    @ApiModelProperty(value = "健康监控URL", name = "healthUrl", example = "http://localhost:80/health", required = false)
    private String healthUrl;
    /**
     * IP地址,多个使用','分割
     */
    @ApiModelProperty(value = "IP地址,多个使用','分割", name = "ipAddress", example = "127.0.0.1:80", required = false)
    private String ipAddress;
    /**
     * 健康监控正则表达式
     */
    @ApiModelProperty(value = "健康监控正则表达式", name = "cron", example = "0 0/3 * * * ?", required = false)
    private String cron;
    /**
     * 备注
     */
    @ApiModelProperty(value = "项目备注", name = "remark", example = "", required = false)
    private String remark;

    public OpsProjectEntity toOpsProjectEntity() {
        OpsProjectEntity opsProjectEntity=new OpsProjectEntity();
        opsProjectEntity.setId(this.getId());
        opsProjectEntity.setAppName(this.getAppName());
        opsProjectEntity.setName(this.getName());
        opsProjectEntity.setExpired(this.getExpired());
        opsProjectEntity.setType(this.getType());
        opsProjectEntity.setHealthUrl(this.getHealthUrl());
        opsProjectEntity.setIpAddress(this.getIpAddress());
        opsProjectEntity.setCron(this.getCron());
        opsProjectEntity.setRemark(this.getRemark());
        return opsProjectEntity;
    }
}
