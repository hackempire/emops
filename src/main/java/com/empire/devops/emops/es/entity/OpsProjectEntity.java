package com.empire.devops.emops.es.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 *
 * </p>
 *
 * @author xutenglong
 * @since 2022-12-27
 */
@Getter
@Setter
@TableName("ops_project")
public class OpsProjectEntity extends Model<OpsProjectEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 项目英文名
     */
    @TableField("app_name")
    private String appName;

    /**
     * 项目中文名
     */
    @TableField("name")
    private String name;

    /**
     * 有效天数
     */
    @TableField("expired")
    private Integer expired;

    /**
     * 类型：1.日志集成;2.监控集成;3.日志+监控集成
     */
    @TableField("type")
    private Integer type;

    /**
     * 健康监控URL
     */
    @TableField("health_url")
    private String healthUrl;

    /**
     * IP地址，多个使用,分割
     */
    @TableField("ip_address")
    private String ipAddress;

    /**
     * 健康状态：1.正常,2.异常
     */
    @TableField("status")
    private Integer status;

    /**
     * 健康监控正则表达式
     */
    @TableField("cron")
    private String cron;

    /**
     * 异常信息
     */
    @TableField("msg")
    private String msg;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
