package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.es.entity.OpsProjectEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * AppItemVO对象
 *
 * @author aaron
 * @date 2021/9/4
 */
@ApiModel(value = "AppItemVO", description = "APP下拉对象")
public class AppItemVO {
    /**
     * app name
     */
    @ApiModelProperty(value = "项目名", name = "appName", example = "\"APP\"")
    private String appName;

    public AppItemVO() {
    }

    public AppItemVO(String appName) {
        this.appName = appName;
    }

    public static AppItemVO newInstance(OpsProjectEntity item) {
        AppItemVO appItemVO=new AppItemVO();
        appItemVO.setAppName(item.getAppName());
        return appItemVO;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public String toString() {
        return "AppItemVO{" +
                "appName='" + appName + '\'' +
                '}';
    }
}
