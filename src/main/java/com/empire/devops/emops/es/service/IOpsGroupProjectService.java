package com.empire.devops.emops.es.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.empire.devops.emops.es.entity.OpsGroupProjectEntity;
import com.empire.devops.emops.es.vo.OpsGroupProjectInfoVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryByGroupVO;
import com.empire.devops.emops.es.vo.OpsGroupProjectQueryPageByGroupVO;
import com.empire.devops.emops.es.vo.OpsProjectDetailVO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
public interface IOpsGroupProjectService extends IService<OpsGroupProjectEntity> {
    /**
     * 分页查询非组内项目列表
     *
     * @param opsGroupProjectQueryPageByGroupVO 项目组项目列表查询对象
     * @return List<OpsProjectDetailVO>  返回项目组不包含的项目列表
     */
    List<OpsProjectDetailVO> queryExcludeListByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO);

    /**
     * 查询非组内项目列表总条数
     *
     * @param opsGroupProjectQueryPageByGroupVO 项目组项目列表查询对象
     * @return 总条数
     */
    Long queryExcludeCountByGroup(OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO);

    /**
     * 分页查询组内项目列表
     *
     * @param osGroupProjectQueryByGroupVO 项目组项目列表查询对象
     * @return List<OpsGroupProjectInfoVO> 返回项目组包含的项目列表
     */
    List<OpsGroupProjectInfoVO> queryIncludeListByGroup(OpsGroupProjectQueryByGroupVO osGroupProjectQueryByGroupVO);
}
