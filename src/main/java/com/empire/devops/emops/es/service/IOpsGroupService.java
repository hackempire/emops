package com.empire.devops.emops.es.service;

import com.empire.devops.emops.es.entity.OpsGroupEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
public interface IOpsGroupService extends IService<OpsGroupEntity> {

}
