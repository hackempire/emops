package com.empire.devops.emops.es.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * CommonQueryVO class 通用查詢對象
 *
 * @author aaron
 * @date 2021/09/05
 */
@ApiModel(value = "CommonQueryVO", description = "通用查询对象")
public class CommonQueryVO extends BasePageQueryVO{
    /**
     * 索引名
     */
    @ApiModelProperty(value = "索引名", name = "idxName",  example = "\"APP-2020-09-07\"")
    private String idxName;
    /**
     * 查询Map封装
     */
    @ApiModelProperty(value = "ES格式查询Map对象", name = "query",  example = "query\":{\"match\":{\"appName\":\"flume-log4j2-em-agent\",\"message\":\"测试\"}}", required = true )
    private Map<String, Map<String, Object>> query;

    public String getIdxName() {
        return idxName;
    }

    public void setIdxName(String idxName) {
        this.idxName = idxName;
    }

    public Map<String, Map<String, Object>> getQuery() {
        return query;
    }

    public void setQuery(Map<String, Map<String, Object>> query) {
        this.query = query;
    }
}
