package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.utils.BasePageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author aaron
 * @Date 2022/12/27
 */
@Data
@ApiModel(value = "OpsProjectQueryVO", description = "项目查询对象")
public class OpsProjectQueryVO extends BasePageQuery implements Serializable {
    /**
     * 项目英文名
     */
    @ApiModelProperty(value = "项目英文名称", name = "appName", example = "log", required = false)
    private String appName;

    /**
     * 项目中文名
     */
    @ApiModelProperty(value = "项目中文名称", name = "name", example = "项目名", required = false)
    private String name;
    /**
     * 有效期
     */
    @ApiModelProperty(value = "日志保留有效期（默认七天）", name = "expired", example = "7", required = false)
    private Integer expired;

    /**
     * 备注
     */
    @ApiModelProperty(value = "项目备注", name = "remark", example = "", required = false)
    private String remark;
    /**
     * IP地址（模糊查询）
     */
    @ApiModelProperty(value = "IP地址（模糊查询）", name = "ipAddress", example = "127.0.0.1:80", required = false)
    private String ipAddress;
}
