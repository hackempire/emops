package com.empire.devops.emops.es.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author aaron
 * @Date 2023/1/1
 */
@Data
@ApiModel(value = "OpsGroupProjectInfoVO", description = "项目组信息对象")
public class OpsGroupProjectInfoVO {
    /**
     * 项目组-项目关联表ID
     */
    @ApiModelProperty(value = "项目组-项目关联表ID", name = "opsGroupProjectId", example = "log", required = true)
    private Integer opsGroupProjectId;
    /**
     * 项目英文名
     */
    @ApiModelProperty(value = "项目英文名称", name = "appName", example = "log", required = true)
    private String appName;

    /**
     * 项目中文名
     */
    @ApiModelProperty(value = "项目中文名称", name = "name", example = "项目名", required = true)
    private String name;
}
