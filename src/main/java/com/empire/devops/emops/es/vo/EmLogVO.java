package com.empire.devops.emops.es.vo;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * emlogVO对象
 *
 * @author aaron
 * @date 2021/9/4
 */
@ApiModel(value = "EmLogVO", description = "日志对象")
public class EmLogVO {
    /**
     * id
     */
    @ApiModelProperty(value = "id", name = "id",  example = "\"BW1AsXsBvdGNm8OWA5rL\"", required = true)
    private String id;
    /**
     * app name
     */
    @ApiModelProperty(value = "项目名", name = "appName",  example = "\"APP\"", required = true)
    private String appName;
    /**
     * 日志级别
     */
    @ApiModelProperty(value = "日志级别", name = "level", example = "\"INFO,DEBUG,WARN,ERROR\"", required = true)
    private String level;
    /**
     * 行数
     */
    @ApiModelProperty(value = "行号", name = "lineNumber",  example = "1006")
    private Integer lineNumber;
    /**
     * logger类名
     */
    @ApiModelProperty(value = "日志类名", name = "loggerName", example = "\"com.empire.emlog.agent.flume.log4j.App\"", required = true)
    private String loggerName;
    /**
     * 日志消息内容
     */
    @ApiModelProperty(value = "日志内容", name = "message", required = true)
    private String message;
    /**
     * 日志方法
     */
    @ApiModelProperty(value = "日志方法", name = "method", required = true)
    private String method;
    /**
     * 日志来源IP
     */
    @ApiModelProperty(value = "服务器IP", name = "sourceIp", required = true)
    private String sourceIp;
    /**
     * 线程名
     */
    @ApiModelProperty(value = "线程名", name = "threadName", required = true)
    private String threadName;
    /**
     * 日志时间
     */
    @ApiModelProperty(value = "日志时间戳", name = "timeMillis", example = "1630766198091")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss.SSS")
    private Date timeMillis;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getLoggerName() {
        return loggerName;
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimeMillis() {
        return timeMillis;
    }

    public void setTimeMillis(Date timeMillis) {
        this.timeMillis = timeMillis;
    }

    @Override
    public String toString() {
        return "EmlogVO{" +
                "id='" + id + '\'' +
                ", appName='" + appName + '\'' +
                ", level='" + level + '\'' +
                ", lineNumber=" + lineNumber +
                ", loggerName='" + loggerName + '\'' +
                ", message='" + message + '\'' +
                ", method='" + method + '\'' +
                ", sourceIp='" + sourceIp + '\'' +
                ", threadName='" + threadName + '\'' +
                ", timeMillis=" + timeMillis +
                '}';
    }
}



