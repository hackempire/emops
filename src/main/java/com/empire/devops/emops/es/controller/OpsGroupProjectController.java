package com.empire.devops.emops.es.controller;

import com.alibaba.fastjson.JSON;
import com.empire.devops.emops.common.BaseResponse;
import com.empire.devops.emops.es.entity.OpsGroupProjectEntity;
import com.empire.devops.emops.es.service.IOpsGroupProjectService;
import com.empire.devops.emops.es.vo.*;
import com.empire.devops.emops.utils.PageResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Api(tags = "项目组管理API")
@ApiSort(4)
@RestController
@RequestMapping("/opsGroupProject")
@Slf4j
public class OpsGroupProjectController {
    private final IOpsGroupProjectService opsGroupProjectService;

    @Autowired
    public OpsGroupProjectController(IOpsGroupProjectService opsGroupProjectService) {
        this.opsGroupProjectService = opsGroupProjectService;
    }

    @ApiOperation(value = "新增项目组项目接口", notes = "新增项目组项目接口")
    @ApiOperationSupport(order = 1)
    @RequestMapping(value = "saveOpsGroupProject", method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {"application/json; charset=UTF-8"})
    public BaseResponse<Void> saveOpsGroupProject(@RequestBody OpsGroupProjectVO opsGroupProjectVO) {
        log.info("saveOpsGroupProject();param:{}", JSON.toJSONString(opsGroupProjectVO));
        try {
            List<OpsGroupProjectEntity> opsGroupEntity = opsGroupProjectVO.toOpsGroupProjectList();
            Date now = new Date();
            opsGroupEntity.forEach(item -> {
                item.setCreateTime(now);
                item.setUpdateTime(now);
            });
//            LambdaQueryWrapper<OpsGroupProjectEntity> opsGroupProjectWrapper = Wrappers.lambdaQuery();
//            opsGroupProjectWrapper.eq(OpsGroupProjectEntity::getGroupId, opsGroupProjectVO.getGroupId());
//            opsGroupProjectService.remove(opsGroupProjectWrapper);
            //新增
            opsGroupProjectService.saveBatch(opsGroupEntity);
        } catch (Exception e) {
            log.error("saveOpsGroupProject()", e);
            return BaseResponse.fail("系统繁忙");
        }
        return BaseResponse.success();
    }

    @ApiOperation(value = "删除项目组项目接口", notes = "删除项目组项目接口")
    @ApiOperationSupport(order = 2)
    @RequestMapping(value = "/deleteOpsGroupProjects", method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {"application/json; charset=UTF-8"})
    public BaseResponse<Void> deleteOpsGroupProjects(@RequestBody List<Integer> ids) {
        log.info("deleteOpsGroupProjects();param:{}", JSON.toJSONString(ids));
        try {
            opsGroupProjectService.removeBatchByIds(ids);
        } catch (Exception e) {
            log.error("deleteOpsGroupProjects()", e);
            return BaseResponse.fail("系统繁忙");
        }
        return BaseResponse.success();
    }

    @ApiOperation(value = "查询非项目组中的项目列表接口", notes = "查询非项目组中的项目列表接口")
    @ApiOperationSupport(order = 3)
    @RequestMapping(value = "queryExcludePageByGroup", method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {"application/json; charset=UTF-8"})
    public BaseResponse<PageResult<OpsProjectDetailVO>> queryExcludePageByGroup(@RequestBody OpsGroupProjectQueryPageByGroupVO opsGroupProjectQueryPageByGroupVO) {
        log.info("queryExcludePageByGroup();param:{}", JSON.toJSONString(opsGroupProjectQueryPageByGroupVO));
        try {
            PageResult<OpsProjectDetailVO> pageResult = new PageResult<>();
            //查询分页列表
            List<OpsProjectDetailVO> list = opsGroupProjectService.queryExcludeListByGroup(opsGroupProjectQueryPageByGroupVO);
            //查询总条数
            Long count = opsGroupProjectService.queryExcludeCountByGroup(opsGroupProjectQueryPageByGroupVO);
            // 两个参数：current的值默认是1，从1开始，不是0。size是每一页的条数。
            pageResult.setList(list);
            pageResult.setCount(count.intValue());
            return BaseResponse.success(pageResult);
        } catch (Exception e) {
            log.error("queryExcludePageByGroup()", e);
            return BaseResponse.fail("系统繁忙");
        }

    }

    @ApiOperation(value = "查询项目组中的项目列表接口", notes = "查询项目组中的项目列表接口")
    @ApiOperationSupport(order = 4)
    @RequestMapping(value = "queryIncludeListByGroup", method = {RequestMethod.POST}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = {"application/json; charset=UTF-8"})
    public BaseResponse<List<OpsGroupProjectInfoVO>> queryIncludeListByGroup(@RequestBody OpsGroupProjectQueryByGroupVO osGroupProjectQueryByGroupVO) {
        log.info("queryIncludeListByGroup();param:{}", JSON.toJSONString(osGroupProjectQueryByGroupVO));
        try {
            List<OpsGroupProjectInfoVO> list = opsGroupProjectService.queryIncludeListByGroup(osGroupProjectQueryByGroupVO);
            return BaseResponse.success(list);
        } catch (Exception e) {
            log.error("queryIncludeListByGroup()", e);
            return BaseResponse.fail("系统繁忙");
        }

    }
}
