package com.empire.devops.emops.es.vo;

import com.empire.devops.emops.es.entity.OpsGroupEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.BeanUtils;

/**
 * @author aaron
 * @Date 2023/1/1
 */
@Data
@ApiModel(value = "OpsGroupVO", description = "新增/更新项目分组对象")
public class OpsGroupVO {

    /**
     * 项目组ID
     */
    @ApiModelProperty(value = "id", name = "id", example = "1", required = false)
    private Integer id;

    /**
     * 分组名称
     */
    @ApiModelProperty(value = "项目分组名称", name = "groupName", example = "", required = true)
    private String groupName;

    /**
     * 邮件，多个使用逗号分隔
     */
    @ApiModelProperty(value = "邮件地址", name = "email", example = "", required = true)
    private String email;

    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息", name = "remark", example = "", required = true)
    private String remark;

    public OpsGroupEntity toOpsGroupEntity() {
        OpsGroupEntity opsGroupEntity = new OpsGroupEntity();
        BeanUtils.copyProperties(this, opsGroupEntity);
        return opsGroupEntity;
    }
}
