package com.empire.devops.emops.es.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author xutenglong
 * @since 2023-01-01
 */
@Getter
@Setter
@TableName("ops_group_project")
public class OpsGroupProjectEntity extends Model<OpsGroupProjectEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 项目组ID
     */
    @TableField("group_id")
    private Integer groupId;

    /**
     * 项目ID
     */
    @TableField("project_id")
    private Integer projectId;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 创建者
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 更新者
     */
    @TableField("update_by")
    private String updateBy;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
