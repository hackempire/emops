package com.empire.devops.emops.common;

/**
 * @author aaron
 * @Date 2023/1/6
 */
public class Constants {
    /**
     * ES通配符*
     */
    public static final String WILDCARD_1 = "*";
    /**
     * ES通配符?
     */
    public static final String WILDCARD_2 = "?";
}
