package com.empire.devops.emops.common;

/**
 * 响应消息Code:Message枚举类
 * 
 * @author auron.xu
 * @date 2020/09/05
 */
public enum MsgRsCodeEnum {
    /*----系统响应----*/
    /**
     * 成功
     */
    LO_SYS_SUCCESS("0", "成功"),
    /**
     * 系统内部错误
     */
    LO_SYS_ERROR("-1", "系统繁忙"),

    /**
     * 必填参数为空/参数错误
     */
    LO_SYS_PARAM_ERROR("40001", "必填参数为空/参数错误"),

    /*----开发者SDK----*/
    /**
     * appId为空
     */
    LO_SYS_APPID_ERROR("40021", "appId为空/无效"),
    /**
     * token为空
     */
    LO_SYS_TOKEN_ERROR("40022", "token为空/无效");

    /**
     * 响应Code
     */
    private String code;
    /**
     * 响应消息
     */
    private String message;

    MsgRsCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
