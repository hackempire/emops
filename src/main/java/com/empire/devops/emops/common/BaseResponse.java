package com.empire.devops.emops.common;


import com.alibaba.fastjson.annotation.JSONField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.StringUtils;

/**
 * 基础响应对象
 *
 * @param <T> 返回数据实体类类型
 * @author auron.xu
 * @date 2021/09/05
 */
@ApiModel(description = "返回响应数据")
public class BaseResponse<T> {
    @ApiModelProperty(value = "响应码")
    private String code;
    @ApiModelProperty(value = "错误信息")
    private String message;
    @ApiModelProperty(value = "返回对象")
    private T data;

    private BaseResponse() {
    }

    private BaseResponse(MsgRsCodeEnum msgRsCodeEnum) {
        this.code = msgRsCodeEnum.getCode();
        this.message = msgRsCodeEnum.getMessage();
    }

    private BaseResponse(MsgRsCodeEnum msgRsCodeEnum, T data) {
        this.code = msgRsCodeEnum.getCode();
        this.message = msgRsCodeEnum.getMessage();
        this.data = data;
    }

    /**
     * 系统错误
     *
     * @param <T> 泛型参数
     * @return 错误响应对象
     */
    public static <T> BaseResponse<T> fail() {
        return new BaseResponse<>(MsgRsCodeEnum.LO_SYS_ERROR);
    }

    /**
     * 返回指定错误信息的响应
     *
     * @param error 错误信息
     * @param <T>   泛型参数
     * @return 错误响应对象
     */
    public static <T> BaseResponse<T> fail(String error) {
        BaseResponse<T> response = fail(MsgRsCodeEnum.LO_SYS_ERROR);
        response.setMessage(error);
        return response;
    }

    /**
     * 返回指定错误码信息的响应
     *
     * @param msgRsCodeEnum 错误码枚举
     * @param <T>           泛型参数
     * @return 错误响应对象
     */
    public static <T> BaseResponse<T> fail(MsgRsCodeEnum msgRsCodeEnum) {
        return fail(msgRsCodeEnum, null);
    }

    /**
     * 返回指定错误码信息的响应
     *
     * @param msgRsCodeEnum 错误码枚举
     * @param error         指定错误消息
     * @param <T>           泛型参数
     * @return 错误响应对象
     */
    public static <T> BaseResponse<T> fail(MsgRsCodeEnum msgRsCodeEnum, String error) {
        BaseResponse<T> response = new BaseResponse(msgRsCodeEnum);
        if (StringUtils.hasLength(error)) {
            response.setMessage(error);
        }
        return response;
    }

    /**
     * 返回成功响应对象
     *
     * @param <T> 泛型参数
     * @return 成功响应对象
     */
    @SuppressWarnings(value = {"rawtypes"})
    public static <T> BaseResponse<T> success() {
        return success(null);
    }

    /**
     * 返回成功数据响应对象
     *
     * @param data 数据
     * @param <T>  泛型参数
     * @return 成功响应对象
     */
    public static <T> BaseResponse<T> success(T data) {
        return new BaseResponse<>(MsgRsCodeEnum.LO_SYS_SUCCESS, data);
    }

    @JSONField(serialize = false)
    public Boolean isSuccess() {
        return MsgRsCodeEnum.LO_SYS_SUCCESS.getCode().equals(this.code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
