package com.empire.devops.emops.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.config.ITypeConvert;
import com.baomidou.mybatisplus.generator.config.converts.*;
import com.baomidou.mybatisplus.generator.config.converts.select.BranchBuilder;
import com.baomidou.mybatisplus.generator.config.converts.select.Selector;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;


/**
 * 因为源码外包无法调用
 * 此文件为复制的 com.baomidou.mybatisplus.generator.config.converts TypeConverts 文件，
 */
public class MyTypeConverts {
    public static ITypeConvert getTypeConvert(DbType dbType) {
        switch (dbType) {
            case ORACLE:
                return OracleTypeConvert.INSTANCE;
            case DB2:
                return DB2TypeConvert.INSTANCE;
            case DM:
            case GAUSS:
                return DmTypeConvert.INSTANCE;
            case KINGBASE_ES:
                return KingbaseESTypeConvert.INSTANCE;
            case OSCAR:
                return OscarTypeConvert.INSTANCE;
            case MYSQL:
            case MARIADB:
                return MySqlTypeConvert.INSTANCE;
            case POSTGRE_SQL:
                return PostgreSqlTypeConvert.INSTANCE;
            case SQLITE:
                return SqliteTypeConvert.INSTANCE;
            case SQL_SERVER:
                return SqlServerTypeConvert.INSTANCE;
            case FIREBIRD:
                return FirebirdTypeConvert.INSTANCE;
            case CLICK_HOUSE:
                return ClickHouseTypeConvert.INSTANCE;
            default:
                return null;
        }
    }

    static Selector<String, IColumnType> use(String param) {
        return new Selector(param.toLowerCase());
    }

    static BranchBuilder<String, IColumnType> contains(CharSequence value) {
        return BranchBuilder.of((s) -> {
            return s.contains(value);
        });
    }

    static BranchBuilder<String, IColumnType> containsAny(CharSequence... values) {
        return BranchBuilder.of((s) -> {
            CharSequence[] var2 = values;
            int var3 = values.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                CharSequence value = var2[var4];
                if (s.contains(value)) {
                    return true;
                }
            }

            return false;
        });
    }
}
