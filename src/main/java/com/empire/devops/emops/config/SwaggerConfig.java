package com.empire.devops.emops.config;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;

/**
 * swagger配置
 * @author xutenglong
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Resource
    private  OpenApiExtensionResolver openApiExtensionResolver;
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .groupName("SwaggerGroupOneAPI")
                //是否开启 (true 开启  false隐藏。生产环境建议隐藏)
                //.enable(false)
                .select()
                //扫描的路径包,设置basePackage会将包下的所有被@Api标记类的所有方法作为api
                .apis(RequestHandlerSelectors.basePackage("com.empire.devops.emops.es.controller"))
                //指定路径处理PathSelectors.any()代表所有的路径
                .paths(PathSelectors.any())
                .build().extensions(openApiExtensionResolver.buildExtensions("global"));
    }
    /**
     * 配置基本信息
     * @return ApiInfo
     */
    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //设置文档标题(API名称)
                .title("emops日志系统")
                //文档描述
                .description("API-接口说明")
                //服务条款URL
                .termsOfServiceUrl("http://localhost:9402/")
                //版本号
                .version("1.0.0")
                //联系人
                .license("").licenseUrl("")
                .contact(new Contact("研发中心", "http://localhost", "aaron@***.com"))
                .build();
    }
}
