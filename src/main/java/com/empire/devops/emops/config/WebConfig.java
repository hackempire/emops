package com.empire.devops.emops.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.OverridesAttribute;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author auron.xu
 * @date 2020/02/14
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    public static final String META_INF_RESOURCES = "classpath:/META-INF/resources/";
    ImmutableMap<String,
        String> webResourceMap = new ImmutableMap.Builder<String, String>().put("doc.html", META_INF_RESOURCES)
            .put("/webjars/**", "classpath:/META-INF/resources/webjars/").put("/service-worker.js", META_INF_RESOURCES)
            .put("/precache-manifest.*.js", META_INF_RESOURCES).build();

//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                // 是否发送Cookie
//                .allowCredentials(true)
//                // 放行哪些原始域
//                .allowedOrigins("*")
//                // 放行哪些请求方式
//                .allowedMethods("GET", "POST", "PUT", "DELETE")
//                // 放行哪些原始请求头部信息
//                .allowedHeaders("Content-Type","X-Requested-With","accept,Origin","Access-Control-Request-Method","Access-Control-Request-Headers","token")
//                 ;
//                // 暴露哪些头部信息
//                //.exposedHeaders("*");
//    }
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        ImmutableSet<Map.Entry<String, String>> webResourceSet = webResourceMap.entrySet();
        webResourceSet.forEach(x -> registry.addResourceHandler(x.getKey()).addResourceLocations(x.getValue()));
    }

    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        // 1.定义一个converters转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverterExtension();
        // 2.添加fastjson的配置信息，比如: 是否需要格式化返回的json数据
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        // 3.在converter中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        // 4.返回HttpMessageConverters对象
        return new HttpMessageConverters(fastConverter);
    }

    public class FastJsonHttpMessageConverterExtension extends FastJsonHttpMessageConverter {
        FastJsonHttpMessageConverterExtension() {
            List<MediaType> mediaTypes = new ArrayList<>();
            mediaTypes.add(MediaType.valueOf(MediaType.TEXT_HTML_VALUE + ";charset=UTF-8"));
            mediaTypes.add(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8"));
            setSupportedMediaTypes(mediaTypes);
        }
    }
}
